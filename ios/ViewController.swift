//
//  ViewController.swift
//  reactnativedocument
//
//  Created by Mikel Diaz on 28/9/21.
//

import UIKit
import Foundation
import VDDocumentCapture

class ViewController: UIViewController, VDDocumentCaptureProtocol {
  
//  public var delegate: VeridasDocumentViewDelegate?
  
  func vdDocumentCaptured(_ imageData: Data!, with captureType: VDCaptureType, andDocument document: [VDDocument]!) {
    
  }
  
  func vdDocumentAllFinished(_ processFinished: Bool) {
    
  }
  
  func vdTimeWithoutPhotoTaken(_ seconds: Int32, with capture: VDCaptureType) {
    
  }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

