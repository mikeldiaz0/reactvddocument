//
//  UserAgent.h
//  VDLibrary
//
//  Created by Veridas on 18/6/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserAgent : NSObject

@property(nonatomic, strong, nonnull) NSString *name;
@property(nonatomic, strong, nonnull) NSString *version;


@end

NS_ASSUME_NONNULL_END
