import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { NativeModules } from "react-native";

export default function App() {
  var StartDocument = NativeModules.StartDocument;
  var StartDocument2 = NativeModules.OpenActivity;

  const openFunc = () => {
    StartDocument.startSDK();
  };

  const openFunc2 = () => {
    StartDocument2.open();
  };

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text>Ejemplo SDK Documento</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          openFunc();
        }}
      >
        <Text style={{ color: "black" }}>Ejecutar SDK Documento</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          openFunc2();
        }}
      >
        <Text style={{ color: "black" }}>Ejecutar Alerta en Android</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    height: 50,
    width: 250,
    backgroundColor: "#f23533",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
  },
});
