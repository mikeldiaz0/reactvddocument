package com.mikeldn.vddocument;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;

import com.dasnano.vddocumentcapture.VDDocumentCapture;
import com.dasnano.vddocumentcapture.config.VDDocumentConfiguration;
import com.dasnano.vddocumentcapture.other.VDDocument;
import com.dasnano.vddocumentcapture.other.VDEnums;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StartDocument extends ReactContextBaseJavaModule implements VDDocumentCapture.IVDDocumentCapture{
    public StartDocument(ReactApplicationContext reactContext){
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "StartDocument";
    }

    @ReactMethod
    public void startSDK(){
        if (!VDDocumentCapture.isStarted()) {
            List<String> spainDocs = new ArrayList<>();
            spainDocs.add("ES_IDCard_2006");
            spainDocs.add("ES_IDCard_2015");
            Map<String, String> configuration = new HashMap<>();
            configuration.put(VDDocumentConfiguration.CLOSE_BUTTON, "YES");
            VDDocumentCapture.startWithDocumentIDs(this, getCurrentActivity(), spainDocs, configuration);
        }
    }

    protected void alerta(String titulo, String texto) {
        AlertDialog alertDialog = new AlertDialog.Builder(getCurrentActivity()).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(texto);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void VDDocumentCaptured(ByteArrayInputStream byteArrayInputStream, VDEnums.VDCaptureType vdCaptureType, List<VDEnums.VDDocumentType> list) {
  
    }

    @Override
    public void VDDocumentCutCaptured(ByteArrayInputStream byteArrayInputStream, VDEnums.VDCaptureType vdCaptureType, List<VDEnums.VDDocumentType> list) {

    }

    @Override
    public void VDDocumentCaptured(byte[] bytes, VDEnums.VDCaptureType vdCaptureType, List<VDDocument> list) {

    }

    @Override
    public void VDDocumentCutCaptured(byte[] bytes, VDEnums.VDCaptureType vdCaptureType, List<VDDocument> list) {

    }

    @Override
    public void VDTimeWithoutPhotoTaken(int i, VDEnums.VDCaptureType vdCaptureType) {

    }

    @Override
    public void VDDocumentCaptureFinished(boolean b) {
        alerta("SDK Documento","Documento Leído");
    }
}
