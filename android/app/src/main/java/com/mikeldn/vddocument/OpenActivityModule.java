package com.mikeldn.vddocument;

import android.app.AlertDialog;
import android.content.DialogInterface;

import androidx.annotation.NonNull;

import com.dasnano.vddocumentcapture.VDDocumentCapture;
import com.dasnano.vddocumentcapture.config.VDDocumentConfiguration;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OpenActivityModule extends ReactContextBaseJavaModule {
    public OpenActivityModule (ReactApplicationContext reactContext){
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "OpenActivity";
    }

    @ReactMethod
    public void open(){
        alerta("aaa","bbb");
    }

    protected void alerta(String titulo, String texto) {
        AlertDialog alertDialog = new AlertDialog.Builder(getCurrentActivity()).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(texto);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
